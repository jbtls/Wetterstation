#include "web_server.h"

AsyncWebServer server(80); ///< Webserver on port 80
//AsyncEventSource events("/events");

void startServer() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    // index.hmtl
    request->send(LittleFS, "/html/index.html", "text/html");
  });

  server.on("/css/pico.slim.min.css", HTTP_GET, [](AsyncWebServerRequest *request){
    // Serve the css
    request->send(LittleFS, "/html/css/pico.slim.min.css", "text/css");
  });

  server.on("/js/apexcharts.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
    // Serve the css
    request->send(LittleFS, "/html/js/apexcharts.min.js", "text/javascript");
  });

  server.on("/js/sensordata.js", HTTP_GET, [](AsyncWebServerRequest *request){
    // Serve the css
    request->send(LittleFS, "/html/js/sensordata.js", "text/javascript");
  });

  // Serve the config page
  server.on("/config.html", HTTP_GET, [](AsyncWebServerRequest *request){
    // Serve the HTML form
    request->send(LittleFS, "/html/config.html", "text/html");
  });

  // Handle all data from the config site and safe it in the corresponding files
  server.on("/config.html", HTTP_POST, [](AsyncWebServerRequest *request){
    // Safe the ssid in ap-mode
    if (request->arg("ssid") != ""){
      // Handle the form submission
      String newSSID = request->arg("ssid");
      Serial.println("New SSID: " + newSSID);
      
      // Open the file for writing
      File file = LittleFS.open(AP_SSID, "w");
      if (!file) {
        Serial.println("Failed to open file for writing");
        request->send(500, "text/plain", "Failed to save SSID");
        return;
      }
      
      // Write the new SSID to the file
      file.println(newSSID);
      file.close();
      
      // Set the new SSID for the access point
      WiFi.softAP(newSSID);
  
      // Send a response to the client
      request->send(200, "text/plain", "SSID saved successfully");
    }

    // Safe wifi credentials
    if (request->arg("wifi-ssid") != "" && request->arg("wifi-password") != ""){
      // Handle the form submission
      String wifiSSID = request->arg("wifi-ssid");
      Serial.println("WiFi-SSID: " + wifiSSID);
      String wifiPassword = request->arg("wifi-password");
      Serial.println("WiFi-Password: *********");
      
      // Open the file for writing
      File file = LittleFS.open(WIFI_FILE, "w");
      if (!file) {
        Serial.println("Failed to open file for writing");
        request->send(500, "text/plain", "Failed to save WiFi-Credentials");
        return;
      }
      
      // Write the new Credentials to the file
      file.println(wifiSSID);
      file.println(wifiPassword);
      file.close();

      Serial.println("Data saved. Try to connect on next startup.");
      
      // Send a response to the client
      request->send(200, "text/plain", "SSID saved successfully");
    }

    // Safe MQTT credentials
    if (request->arg("mqtt-server") != "" && request->arg("mqtt-topic") != "" && request->arg("mqtt-port") != ""){
      // Handle the form submission
      String mqttServer = request->arg("mqtt-server");
      Serial.println("MQTT-Server: " + mqttServer);
      String mqttPort = request->arg("mqtt-port");
      Serial.println("MQTT-Port: " + mqttPort);
      String mqttTopic = request->arg("mqtt-topic");
      Serial.println("MQTT-Topic: " + mqttTopic);

      // Open the file for writing
      File file = LittleFS.open(MQTT_FILE, "w");
      if (!file) {
        Serial.println("Failed to open file for writing");
        request->send(500, "text/plain", "Failed to save MQTT-Credentials");
        return;
      }

      // Write the new Credentials to the file
      file.println(mqttServer);
      file.println(mqttPort);
      file.println(mqttTopic);

      // If a user and a password is provided safe them also (not required)
      if (request->arg("mqtt-user") != "" && request->arg("mqtt-password") != ""){
        String mqttUser = request->arg("mqtt-user");
        Serial.println("MQTT-User: " + mqttUser);
        String mqttPassword = request->arg("mqtt-password");
        Serial.println("MQTT-Password: " + mqttPassword);
      }

      file.close();

      Serial.println("Data saved. Try to connect on next startup.");
      
      // Send a response to the client
      request->send(200, "text/plain", "MQTT credentials saved successfully");
    }
  });

  // Send the sensor-data as json
  server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request){
    String temp = String(dht22Temp);
    String humidity = String(dht22Humidity);
    String pressure = String(bmp280Pressure);
    
    String json = "{\"temp\": \"" + temp + "\", \"humidity\": \"" + humidity + "\", \"pressure\": \"" + pressure + "\"}";
    Serial.println(json);
    
    request->send(200, "application/json", json);
  });

  // Start the server
  server.begin();
}