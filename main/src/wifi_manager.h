/**
 * @file wifi_manager.h
 * @brief Contains functions to start up an ap and connect an existing wifi if possible.
 */

#ifndef WIFI_MANAGER_H
#define WIFI_MANAGER_H

#include <Arduino.h>
#include <WiFi.h>
#include <LittleFS.h>
#include "config.h"
#include "global.h"

extern String espID;

/**
 * @brief Tries to connect to a Wi-Fi network by reading the credentials from a file.
 * 
 * @return True if the connection to the Wi-Fi network was successful, false otherwise.
 */
bool connectWiFi();

/**
 * @brief Starts the access point and sets up the SSID.
 *
 * The function sets the mode of the WiFi to access point and checks if the SSID file exists
 * and is not empty. If it exists and is not empty, it reads the SSID from the file and sets
 * it as the access point SSID. If it does not exist or is empty, it sets the SSID to a default
 * value and creates a new SSID file.
 *
 * @return void
 */
void startAccessPoint();

#endif // WIFI_MANAGER_H