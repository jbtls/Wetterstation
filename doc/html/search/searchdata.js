var indexSectionsWithContent =
{
  0: "abcdeglmprsw",
  1: "cglmsw",
  2: "cdlmprs",
  3: "bdems",
  4: "abdmw",
  5: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros",
  5: "Pages"
};

