/**
 * @file little_fs.h
 * @brief Some basic functions for a working filesystem on the ESP32.
 */

#ifndef LITTLE_FS_H
#define LITTLE_FS_H

#include <Arduino.h>
#include <LittleFS.h>
#include "config.h"
#include "global.h"

/**
 * @brief Count lines in file. The filename needs to be provided.
 *
 * This function counts the lines provided in a file.
 * 
 * @return int
 * 
 */
int countLines(String fileName);

/**
 * @brief Initializes the LittleFS filesystem
 *
 * This function initializes the LittleFS filesystem and formats it if it failed to mount.
 * If formatting is successful, a success message is printed to the Serial monitor. If it fails,
 * an error message is printed and the function returns.
 *
 */
void setupLittleFS();

#endif // LITTLE_FS_H
