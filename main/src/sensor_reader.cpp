#include "sensor_reader.h"

DHT dht(DHTPIN, DHTTYPE); 
Adafruit_BMP280 bmp; 
//Adafruit_BMP280 bmp(BMP_CS); ///< Hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK); ///< Software SPI

void setupSensors(){
  dht.begin();

  if (!bmp.begin(0x76)) {  
    Serial.println("Could not find a valid BMP280 !");
  }
}

void readBMP280(){
  bmp280Temp = bmp.readTemperature();
  bmp280Pressure = bmp.readPressure()/100;
  bmp280Altitude = bmp.readAltitude(1025.4);
  
  Serial.print("T = ");
  Serial.print(bmp280Temp);
  Serial.println(" °C");
  
  Serial.print("P = ");
  Serial.print(bmp280Pressure);
  Serial.println(" hPa ");

  Serial.print("H = ");
  Serial.print(bmp280Altitude); 
  Serial.println(" m");
}

void readDHT22(){
  dht22Humidity = dht.readHumidity(); 
  dht22Temp = dht.readTemperature();
  
  Serial.print("Humidity: "); 
  Serial.print(dht22Humidity);
  Serial.println(" %");
  Serial.print("Temperature: ");
  Serial.print(dht22Temp);
  Serial.println(" °C");
}