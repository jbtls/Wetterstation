#include <Arduino.h>

String espID = String(ESP.getEfuseMac());

float dht22Humidity = 0;
float dht22Temp = 0;
float bmp280Temp = 0;
float bmp280Pressure = 0;
float bmp280Altitude = 0;

String mqttBroker;
String mqttPort;
String mqttTopic;
String mqttUser;
String mqttPassword;