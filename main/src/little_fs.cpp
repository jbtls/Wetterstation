#include "little_fs.h"

int countLines(String fileName){
  File file = LittleFS.open(fileName, "r");
  int lines = 0;
  
  if (!file || file.size() < 0) {
    return 0;
  }
  
  while (file.available()) {
    char c = file.read();
    Serial.print(c);
    if (c == '\n') {
      lines++;
    }
  }

  file.close();
  return lines;
}

void setupLittleFS() {
  if (!LittleFS.begin()) {
    Serial.println("LittleFS Mount Failed");

    // Format LittleFS if it failed to mount
    if (LittleFS.format()) {
      Serial.println("LittleFS Formatting Succeded");           
    } else {
      Serial.println("LittleFS Formatting Failed");
    }
  }
}