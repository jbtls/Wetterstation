#include "mqtt_client.h"

// MQTT client
WiFiClient espClient;
PubSubClient mqttClient(espClient);

void connectMQTT(){  
  File file = LittleFS.open(MQTT_FILE, "r");

  int lines = countLines(MQTT_FILE);

  // Check if the file has the correct number of credentials
  if (lines == 3 || lines == 5) {
    // read file until the end of the first line
    mqttBroker = file.readStringUntil('\n');
    mqttBroker.trim();
    mqttPort = file.readStringUntil('\n');
    mqttPort.trim();
    mqttTopic = file.readStringUntil('\n');
    mqttTopic.trim();

    // If user and password is also stored use them
    if (lines == 5) {
      mqttUser = file.readStringUntil('\n');
      mqttUser.trim();
      mqttPassword = file.readStringUntil('\n');
      mqttPassword.trim();
    }

    mqttClient.setServer(mqttBroker.c_str(), mqttPort.toInt());
    
    // Connect to broker
    for(int i=0; i<5 && !mqttClient.connected(); i++) {
      Serial.print("Attempting MQTT connection...");
      if (lines == 3){
        mqttClient.connect("ESP32Client");
      } else {
        mqttClient.connect("ESP32Client", mqttUser.c_str(), mqttPassword.c_str());
      }
      if (mqttClient.connected()) {
        Serial.println("connected");
      } else {
        Serial.print("failed, rc=");
        Serial.print(mqttClient.state());
        Serial.println(" try again in 5 seconds");
        delay(5000);
      }
    }
  } else {
    Serial.println("No valid mqtt credentials found.");
  }
  
  file.close();
}

void publishMQTT(){
  if (mqttClient.connected()){
    // Publish DHT22 data
    mqttClient.publish((mqttTopic + "/dht22Humidity").c_str(), String(dht22Humidity).c_str());
    mqttClient.publish((mqttTopic + "/dht22Temp").c_str(), String(dht22Temp).c_str());
    
    // Publish BMP280 data
    mqttClient.publish((mqttTopic + "/bmp280Temp").c_str(), String(bmp280Temp).c_str());
    mqttClient.publish((mqttTopic + "/bmp280Pressure").c_str(), String(bmp280Pressure).c_str());
    mqttClient.publish((mqttTopic + "/bmp280Altitude").c_str(), String(bmp280Altitude).c_str());
  }
}
