/**
 * @file mqtt_client.h
 * @brief Contains functions to connect and publish data to an MQTT broker.
 */

#ifndef MQTT_CLIENT_H
#define MQTT_CLIENT_H

#include <Arduino.h>
#include <LittleFS.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include "config.h"
#include "global.h"
#include "little_fs.h"

extern WiFiClient espClient;
extern PubSubClient mqttClient;

/**
 * @brief Connect to the MQTT broker
 *
 * The function connects to an mqtt broker using the credentials provided in the MQTT_FILE.
 * If a user and a password is stored in the file it tries to connect with this credentials.
 * After five attempts it stops trying to connect.
 *
 * @return void
 */
void connectMQTT();

/**
 * Publishes DHT22 and BMP280 sensor data to the MQTT broker if the client is connected.
 *
 * @param None
 * @return None
 */
void publishMQTT();

#endif // MQTT_CLIENT_H