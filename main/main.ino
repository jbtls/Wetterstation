/**
 * @file main.cpp
 * @mainpage ESP32 Weather Station with LittleFS and AsyncWebServer
 *
 * This code implements an ESP32-based weather station using the LittleFS file system and the AsyncWebServer library. 
 * It sets up an access point with an SSID stored in a LittleFS file, and allows a user to change the SSID through a web interface. 
 * This code is intended for use with an ESP32 development board.
 *
 * @author Justin Behmel
 *
 * @date 25.04.2023
 *
 * @note This code depends on the following libraries:
 *  - WiFi
 *  - ESPAsyncWebServer 1.2.6: https://github.com/me-no-dev/ESPAsyncWebServer
 *  - LittleFS
 *  - ArduinoIDE 1.8.19: 
 *  - PubSubClient 2.8.0: https://pubsubclient.knolleary.net/
 *  - Arduino ESP32 filesystem uploader 2.0.7: https://github.com/lorol/arduino-esp32fs-plugin
 *  - Adafruit_Sensor
 *  - Adafruit_BMP280 2.6.6
 */

#include <Arduino.h>
#include "src/config.h"
#include "src/global.h"
#include "src/little_fs.h"
#include "src/wifi_manager.h"
#include "src/web_server.h"
#include "src/mqtt_client.h"
#include "src/sensor_reader.h"

void setup() {
  Serial.begin(115200);
  
  setupLittleFS();
  if(!connectWiFi()){
    startAccessPoint();
  } else {
    connectMQTT();
  }
  
  startServer();
  setupSensors();
}

void loop() {
  readDHT22();
  readBMP280();
  publishMQTT();
  delay(10000);
}
