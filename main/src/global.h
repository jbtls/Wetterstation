/**
 * @file globals.h
 * @brief Header file containing global variable declarations.
 *
 * This file contains declarations of global variables used in the project.
 */

#ifndef SENSOR_VARIABLES_H
#define SENSOR_VARIABLES_H

#include <Arduino.h>

extern String espID;             ///< Unique ID of the ESP device.

extern float dht22Humidity;      ///< Humidity value from DHT22 sensor.
extern float dht22Temp;          ///< Temperature value from DHT22 sensor.
extern float bmp280Temp;         ///< Temperature value from BMP280 sensor.
extern float bmp280Pressure;     ///< Pressure value from BMP280 sensor.
extern float bmp280Altitude;     ///< Altitude value from BMP280 sensor.

extern String mqttBroker;        ///< MQTT broker address.
extern String mqttPort;          ///< MQTT broker port.
extern String mqttTopic;         ///< MQTT topic for publishing data.
extern String mqttUser;          ///< MQTT username for authentication.
extern String mqttPassword;      ///< MQTT password for authentication.

#endif // SENSOR_VARIABLES_H
