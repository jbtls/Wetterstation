/**
 * @file web_server.h
 * @brief Contains functions to start up the web server and handle requests.
 */

#ifndef WEB_SERVER_H
#define WEB_SERVER_H

#include <Arduino.h>
#include <ESPAsyncWebSrv.h>
#include <LittleFS.h>
#include "config.h"
#include "global.h"

extern AsyncWebServer server;

/**
 * @brief Starts the web server and sets up handlers for HTTP GET and POST requests.
 *
 * The function sets up the server to handle HTTP GET and POST requests. The HTTP GET
 * request serves the HTML form, while the HTTP POST request handles the form submission.
 *
 * @return void
 */
void startServer();

#endif // WEB_SERVER_H