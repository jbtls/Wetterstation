# Configuration Schema
## Release
In the current release only one network is stored in the data folder. The structure is like this:
```
./data/
+-- ./ap-ssid.txt
+-- ./client-wifi.txt
+-- ./mqtt.txt
```

 - `ap-ssid.txt` stores the data for the basic startup as an Access-Point.
 - `client-wifi.txt` stores the data for an connection to an existing wifi. If the Wifi is not present, the `ap-ssid.txt` is used as fallback to start in AP-Mode.
 - `mqtt.txt` stores the data for the connection to an MQTT-Server. This is only possible if the ESP is in Client-Mode.

### ap-ssid.txt
The file is structured as follows:
```
SSID
```

### client-wifi.txt
The file is structured as follows:
```
SSID
PASSWORD
```

### mqtt.txt
The file is structured as follows:
```
SERVER
PORT
TOPIC
(USERNAME)
(PASSWORD)
```

## Future Planings
The following schema should be used to configure an ESP device in future. The configuration is stored in a JSON file.

```json
{
  "mqtt": {
    "server": "string",
    "port": "integer",
    "topic": "string",
    "username": "string",
    "password": "string"
  },
  "wifi": {
    "networks": [
      {
        "ssid": "string",
        "password": "string"
      },
      {
        "ssid": "string",
        "password": "string"
      }
    ],
    "ap": {
      "ssid": "string",
      "password": "string"
    }
  }
}
```
### mqtt

This object contains the MQTT configuration. The fields are as follows:

- `server`: The MQTT broker address (string)
- `port`: The MQTT broker port (integer)
- `topic`: The MQTT topic where the sensor data should be published (String)
- `username`: The username used for authentication (string)
- `password`: The password used for authentication (string)

### wifi

This object contains the Wi-Fi configuration. The fields are as follows:

- `networks`: An array of networks. Each network is an object with the following fields:
    - `ssid`: The SSID of the Wi-Fi network (string)
    - `password`: The password for the Wi-Fi network (string)
- `ap`: An object containing the configuration for the ESP's access point (AP):
    - `ssid`: The SSID of the AP (string)
    - `password`: The password for the AP (string)

The ESP will attempt to connect to the networks listed in the `networks` array. If no networks are in range, the ESP will start in AP mode with the SSID and password specified in the `ap` object.
