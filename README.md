# Wetterstation

## Beschreibung

Entwicklung einer modularen Wetterstation, im Rahmen des Lernfelds 07 (Rahmenlehrplan für die
Ausbildungsberufe Fachinformatiker und Fachinformatikerin IT-System-Elektroniker und IT-System-Elektronikerin) "Cyber-physische Systeme ergänzen".

## Credits
Auch wenn nicht jede hier genannte Software Teil dieses Projekts ist, wurden alle erwähnten Teile genutzt, um diese Software zu erstellen. An dieser Stelle vielen Dank an:
- [Pico.css v1.5.7](https://picocss.com/) - MIT Lizenz
- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer) - Keine Lizenz angegeben
- [ApexCharts](https://github.com/apexcharts/apexcharts.js)  - MIT Lizenz
- [littlefs](https://github.com/littlefs-project/littlefs) - BSD-3-Clause Lizenz
- [Arduino ESP32 filesystem uploader](https://github.com/lorol/arduino-esp32fs-plugin) - GPL-2.0 Lizenz

## Lizenz
MIT-Lizenz. Hilfe zu Lizenzen finden Sie hier: [https://choosealicense.com/](https://choosealicense.com/)
