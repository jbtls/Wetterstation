#include "wifi_manager.h"

bool connectWiFi() {
  bool success = false;
  
  // Set the Wi-Fi mode to station mode
  WiFi.mode(WIFI_STA);

  // Open the Wi-Fi configuration file
  File wifiFile = LittleFS.open(WIFI_FILE, "r");
  
  if (wifiFile && wifiFile.size() > 0) {
    // Read the SSID and password from the configuration file
    String ssid = wifiFile.readStringUntil('\n');
    ssid.trim();
    String password = wifiFile.readStringUntil('\n');
    password.trim();
    
    wifiFile.close();
    
    Serial.print("Connecting to ");
    Serial.println(ssid);

    // Set a custom hostname
    String hostname = "Wetterstation-" + espID;
    WiFi.setHostname(hostname.c_str());
    
    // Attempt to connect to the Wi-Fi network
    WiFi.begin(ssid.c_str(), password.c_str());

    int timeout = 0;
    
    // Wait for the connection to be established
    while (WiFi.status() != WL_CONNECTED && timeout < 10) {
      delay(500);
      Serial.print(".");
      timeout++;
    }

    // Check if the connection was successful
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("Failed to connect to Wi-Fi.");
    } else {
      Serial.println("Connected to Wi-Fi.");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      success = true;
    }
  } else {
    Serial.println("Failed to open Wi-Fi configuration file."); 
  }

  // Return whether the connection was successful or not
  return success;
}

void startAccessPoint() {
  WiFi.mode(WIFI_AP);
  // set the SSID to "Wetterstation-<ESPID>"
  String ssid = "Wetterstation-" + espID;

  // check if ssid.txt exists and is not empty
  File file = LittleFS.open(AP_SSID, "r");
  if (file && file.size() > 0) {
    // read file until the end of the first line
    ssid = file.readStringUntil('\n');
    ssid.trim();
    file.close();
  } else {
    // create and write the SSID to the ssid.txt file
    file = LittleFS.open(AP_SSID, "w");
    if (!file) {
      Serial.println("Failed to open file for writing");
      return;
    }
    file.print(ssid);
    file.close();
  }
  WiFi.softAP(ssid);

  Serial.print("Access Point SSID: ");
  Serial.println(WiFi.softAPSSID());
  Serial.println("Access point started");
}