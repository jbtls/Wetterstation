/**
 * @file sensor_reader.h
 * @brief Contains functions to setup the used sensors and read data from them.
 */

#ifndef SENSOR_READER_H
#define SENSOR_READER_H

#include <Arduino.h>
#include <DHT.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include "config.h"
#include "global.h"

extern DHT dht;                 ///< DHT22 Object

/**
 * @brief External declaration of Adafruit_BMP280 object for BMP280 sensor
 * 
 * The object declaration is used to access the BMP280 sensor data in other parts of the code.
 * This specific declaration uses I2C communication to connect with the sensor.
 * If the user wants to use SPI communication, they can use the following code instead:
 * 
 * @code
 * Adafruit_BMP280 bmp(BMP_CS); ///< Hardware SPI
 * Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO, BMP_SCK); ///< Software SPI
 * @endcode
*/

extern Adafruit_BMP280 bmp;

/**
 * @brief Sets up the sensors used in the project.
 * 
 * This function initializes the sensors that are being used in the project.
 * New sensors should be initialized within this function if they are being used.
 */
void setupSensors();

/**
 * @brief Reads the temperature and pressure values from the BMP280 sensor.
 * 
 * This function reads the temperature and pressure values from the BMP280 sensor
 * and stores them in global variables. It uses the Adafruit_BMP280 library to communicate
 * with the sensor and read its values.
 *
 * @note This function assumes that the BMP280 sensor is connected to the I2C or the SPI interface
 * of the ESP. You have to set it up in the beginning of the file.
 * 
 * @return void
 */
void readBMP280();

/**
 * @brief Reads the temperature and humidity values from the DHT22 sensor.
 * 
 * This function reads the temperature and humidity values from the DHT22 sensor
 * and stores them in global variables. It uses the DHT library to communicate with
 * the sensor and read its values.
 *
 * @note This function assumes that the DHT22 sensor is connected to the digital
 * pin 2 of the Arduino board.
 * 
 * @return void
 */
void readDHT22();

#endif // SENSOR_READER_H