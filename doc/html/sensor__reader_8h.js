var sensor__reader_8h =
[
    [ "readBMP280", "sensor__reader_8h.html#a590c773e910aa4fe76fadc6b2ccc4881", null ],
    [ "readDHT22", "sensor__reader_8h.html#aacc9e88cccabea6b433bd636d4e63f0e", null ],
    [ "setupSensors", "sensor__reader_8h.html#a1caf8bfd08a8683cc5d3f674796b0990", null ],
    [ "bmp", "sensor__reader_8h.html#a841b3452103a9a56bc1c17e959adfa7a", null ],
    [ "dht", "sensor__reader_8h.html#a36deb824329db8b719c4f8f2ca8e83cc", null ]
];