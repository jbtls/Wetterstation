/**
 * @file sensor_definitions.h
 * @brief Defines basic constants and pins for the sensors used in the project.
 * This file contains basic constants and pin definitions for the sensors used in the project.
 * It defines the pins for the DHT22 sensor and the BMP280 sensor (both for I2C and SPI connection).
 * It also defines constants for file names to read from and to write to.
 * These constants are used in the file reading and writing functions.
 */

#ifndef CONFIG_H
#define CONFIG_H

// Some basic defines
#define AP_SSID "/ap-ssid.txt"          ///< File name for the access point SSID
#define WIFI_FILE "/client-wifi.txt"    ///< File name for the Wi-Fi configuration
#define MQTT_FILE "/mqtt.txt"           ///< File name for the MQTT configuration

// Define sensor pins
// Define Pins for the DHT22
#define DHTPIN 4                        ///< Pin number for the DHT22 sensor   
#define DHTTYPE DHT22                   ///< Specific type of the DHT sensor

// Define Pins for the BMP280
// BMP280 basics: https://learn.adafruit.com/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout/downloads?view=all
// BMP280 I2C connection
#define BMP_SDA 21                      ///< Pin number for I2C data line of BMP280
#define BMP_SCL 22                      ///< Pin number for I2C clock line of BMP280
// BMP280 SPI connection
#define BMP_SCK 13                      ///< Pin number for SPI clock line of BMP280
#define BMP_MISO 12                     ///< Pin number for SPI MISO line of BMP280
#define BMP_MOSI 14                     ///< Pin number for SPI MOSI line of BMP280
#define BMP_CS 10                       ///< Pin number for BMP280 chip select line

#endif // CONFIG_H