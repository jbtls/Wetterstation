document.addEventListener("DOMContentLoaded", function(event) {

	// Get references to the buttons
	var tempBtn = document.getElementById('temp-btn');
	var humidityBtn = document.getElementById('humidity-btn');
	var pressureBtn = document.getElementById('pressure-btn');

	// Function to fetch the sensor data and update the buttons
	function updateSensorData() {
	  fetch('/data')
	    .then(response => response.json())
	    .then(data => {
	      // Update the captions of the buttons
	      tempBtn.textContent = 'Temp: ' + data.temp + ' °C';
      	  humidityBtn.textContent = 'Humidity: ' + data.humidity + ' %';
	      pressureBtn.textContent = 'Pressure: ' + data.pressure + ' hPa';
	    });
	}

	// Call the function to update the buttons initially
	updateSensorData();

	// Call the function every 5 seconds to update the buttons
	setInterval(updateSensorData, 5000);

});